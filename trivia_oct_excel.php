<?php
$servername = "localhost";
$username = "santia45_tritele";
$password = "catch33";
$dbname = "santia45_trivia_nueva_telefonica";

if (PHP_SAPI == 'cli'){
  die("La ejecucion solo puede ser a traves del navegador");
}

// require the excel lib from ./PHPExcel
require_once './PHPExcel.php';

// Creating object PHPExcel
$objPHPExcel = new PHPExcel();

// properties of the document.

$objPHPExcel->getProperties()->setCreator("Nicolas Riquelme")
                            ->setLastModifiedBy("Nicolas Riquelme")
                            ->setTitle("Office XLSX Documento de Prueba")
                            ->setSubject("Office XLSX Documento de Prueba")
                            ->setDescription("Documento de prueba Excel")
                            ->setKeywords("office 2010 openxml php")
                            ->setCategory("Archivo con resultado de prueba");

// combine cells from A1 to E1
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'REPORTE TRIVIA')
            ->setCellValue('A2', 'ID')
            ->setCellValue('B2', 'NOMBRE EMPRESA')
            ->setCellValue('C2', 'RUT')
            ->setCellValue('D2', 'EMAIL TRABAJO')
            ->setCellValue('E2', 'TELEFONO')
            ->setCellValue('F2', 'NOMBRE PERSONA')
            ->setCellValue('G2', 'ÁREA TRABAJO')
            ->setCellValue('H2', 'CARGO')
            ->setCellValue('I2', '¿TIENE ALGÚN PRODUCTO CON TELEFÓNICA?')
            ->setCellValue('J2', '¿QUÉ PRODUCTOS CONSIDERAS QUE NECESITA TU EMPRESA?')
            ->setCellValue('K2', '¿LE INTERESA QUE LES ENVIEMOS EL NEWSLETTER DE EMPRESAS?')
            ->setCellValue('L2', '¿HAY ALGÚN OTRO CONTACTO AL QUE DEBERÍAMOS ENVIAR EL NEWSLETTER?')
            ->setCellValue('M2', "FECHA INGRESO");



// the font for the first row will be black
$boldArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

$objPHPExcel->getActiveSheet()->getStyle('A1:E2')->applyFromArray($boldArray);

// width of the columns

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);

// Now we connect to the database
// $conn = @mysqli_connect('localhost:3306', 'nrriquel', 'Nrriquel1987', 'santiago_lab');
$conn = new mysqli($servername, $username, $password, $dbname);
if (!$conn){
  die("imposible conectarse " . mysqli_error($conn));
}
if (@mysqli_connect_errno()){
  die("Connect failed: " . mysqli_connect_errno() . " : "  . mysqli_connect_error());
}

// $sql = "SELECT * FROM trivia_nueva order by id";
$sql = "SELECT * FROM trivia_octubre order by id";

$query = mysqli_query($conn, $sql);

$cel = 3; // number of the row that will start the creation of the report

// fetching all the rows
while ($row = mysqli_fetch_array($query)){
  $id = $row['id'];
  $nombre = $row['nombre'];
  $rut = $row['rut'];
  $email_trabajo = $row['email_trabajo'];
  $telefono = $row['telefono'];
  $nombre_persona = $row['nombre_per'];
  $area_trabajo = $row['area_trabajo'];
  $cargo = $row['cargo'];
  $preg1 = $row['p1'];
  $preg2 = $row['p2'];
  $preg3 = $row['p3'];
  $preg4 = $row['p3'];
  $fecha = $row['fecha_ingreso'];

  $a = 'A'.$cel;
  $b = 'B'.$cel;
  $c = 'C'.$cel;
  $d = 'D'.$cel;
  $e = 'E'.$cel;
  $f = 'F'.$cel;
  $g = 'G'.$cel;
  $h = 'H'.$cel;
  $i = 'I'.$cel;
  $j = 'J'.$cel;
  $k = 'K'.$cel;
  $l = 'L'.$cel;
  $m = 'M'.$cel;

  // Adding the data

  $objPHPExcel->setActiveSheetIndex(0)
  ->setCellValue($a, $id)
  ->setCellValue($b, $nombre)
  ->setCellValue($c, $rut)
  ->setCellValue($d, $email_trabajo)
  ->setCellValue($e, $telefono)
  ->setCellValue($f, $nombre_persona)
  ->setCellValue($g, $area_trabajo)
  ->setCellValue($h, $cargo)
  ->setCellValue($i, $preg1)
  ->setCellValue($j, $preg2)
  ->setCellValue($k, $preg3)
  ->setCellValue($l, $preg4)
  ->setCellValue($m, $fecha);


  $cel+=1;

  // end data extraction
}

$rango = "A2:$m";

$styleArray = array('font' => array('name' => 'Arial', 'size' => 10),
'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FFF'))));

$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);

// few more details => name of the sheet

$objPHPExcel->getActiveSheet()->setTitle('Reporte Trivia Telefonica');
$objPHPExcel->setActiveSheetIndex(0);

// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="reporte_trivia_telefonica.xls"');
header('Cache-Control: max-age=0');
// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
 ?>
